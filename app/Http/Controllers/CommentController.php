<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Comment;

class CommentController extends Controller
{
    public function store(Request $request){
    	//Validate form
    	
    	$validate = $this->validate($request, ['body' => 'required'] );

    	$comment = new Comment();
    	$user = \Auth::user();

    	$comment->user_id = $user->id;
    	$comment->video_id = $request->input('videoId');
    	$comment->body = $request->input('body');

    	$comment->save();

    	return redirect()->route('detailVideo', 
    		['videoId' => $comment->video_id])->with(array('message' => 'Comment has been added successfully'));

    }

    public function delete($comment_id){
        $user = \Auth::user();
        $comment = Comment::find($comment_id);

        if($user && ($comment->user_id == $user->id || $comment->video->user_id == $user->id)){
            $comment->delete();
        }

        return redirect()->route('detailVideo', 
            ['videoId' => $comment->video_id])->with(array('message' => 'Comment has been deleted successfully'));
    }
}
