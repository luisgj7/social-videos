<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

use App\Video;
use App\Comment;

class VideoController extends Controller
{
    public function createVideo(){
    	return view('video.createVideo');
    }

    public function saveVideo(Request $request){

    	//Validate Form
    	$validateData = $this->validate($request, [
    		'title' => 'required|min:5', 
    		'description' => 'required', 
    		'video' => 'mimes:mp4'
    	]);

        //Get Request
        $video = new Video();
        $user = \Auth::user();
        $video->user_id = $user->id;
        $video->title = $request->input('title');
        $video->description = $request->input('description');

        //Upload Image Tumbname
        $image = $request->file('image');
        if ($image) {
           //$image_path = time().$image->getClientOriginalName();
           
           $file_extension = $image->getClientOriginalExtension(); 
           $filename = rand(11111111, 99999999) . '.' . $file_extension;
           \Storage::disk('images')->put($filename, \File::get($image));

           $video->imagen = $filename;
        }

        //Upload Video
        $video_file = $request->file('video');

        if ($video_file) {
           //$video_path = time().$video_file->getClientOriginalName();
           
           $file_extension = $video_file->getClientOriginalExtension(); 
           $filename = rand(11111111, 99999999) . '.' . $file_extension;
           \Storage::disk('videos')->put($filename, \File::get($video_file));

           $video->video_path = $filename;
        }

        $video->save();

        return redirect()->route('home')->with(array('message' => 'the video has benn uploaded successfully'));
    }


    public function getImage($filename){
        $file = Storage::disk('images')->get($filename);
        return new Response($file, 200);
    }

    public function getVideoDetail($videoId){
        $video = Video::find($videoId);
        return view('video.detail', array('video' => $video));
    }

    public function getVideo($filename){
        $file = Storage::disk('videos')->get($filename);
        return new Response($file, 200);
    }

    public function delete($video_id){
        $user = \Auth::user();
        $video = Video::find($video_id);

        $comments = Comment::where('video_id', $video_id)->get();

        if($user && $video->user_id == $user->id){

            //Delete comments
            if($comments && count($comments) >= 1){
                foreach ($comments as $comment) {
                     $comment->delete();
                }
            }
           
            //Delete files
            Storage::disk('images')->delete($video->imagen);
            Storage::disk('videos')->delete($video->video_path);

            //Delete video
            $video->delete();

            $message = array('message' => 'video has been deleted successfully');

        }else{
            $message = array('message' => 'Ops! the video could not be deleted');
        }

        return redirect()->route('home')->with($message);
    }

    public function edit($video_id){
        $video = Video::findOrFail($video_id);

        return view('video.edit', array('video' => $video));
    }

    public function update($video_id, Request $request){
        //Validate Form
        $validateData = $this->validate($request, [
            'title' => 'required|min:5', 
            'description' => 'required', 
            'video' => 'mimes:mp4'
        ]);

        //Get Request
        $video = Video::findOrFail($video_id);
        $user = \Auth::user();
        $video->user_id = $user->id;
        $video->title = $request->input('title');
        $video->description = $request->input('description');


            
        //Upload Image Tumbname
        $image = $request->file('image');
        if ($image) {
           //$image_path = time().$image->getClientOriginalName();
           
           $file_extension = $image->getClientOriginalExtension(); 
           $filename = rand(11111111, 99999999) . '.' . $file_extension;
           Storage::disk('images')->put($filename, \File::get($image));

           if($video->imagen){
            Storage::disk('images')->delete($video->imagen);
           }
           
           $video->imagen = $filename;
        }

        //Upload Video
        $video_file = $request->file('video');

        if ($video_file) {
           //$video_path = time().$video_file->getClientOriginalName();
           
           $file_extension = $video_file->getClientOriginalExtension(); 
           $filename = rand(11111111, 99999999) . '.' . $file_extension;
           Storage::disk('videos')->put($filename, \File::get($video_file));
           

           if($video->video_path){
            Storage::disk('videos')->delete($video->video_path);
           }
           
           $video->video_path = $filename;
        }

        $video->update();

        return redirect()->route('home')->with(array('message' => 'the video has been updated successfully'));
    }

    public function search(Request $request, $param = null){

        if(is_null($param)){
            $param = $request->get('param');

            return redirect()->route('searchVideo' array('param' => $param));
        }
        
        $videos = Video::where('title', 'LIKE', '%'. $param . '%')->paginate(5);

        return view('video.search', array(
                                           'videos' => $videos,
                                           'search' => $param ));
    }
}
