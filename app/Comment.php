<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $table = 'comments';
    
    //Relation Many to One
    //Many comments belong to a user
   	public function user(){
   		return $this->belongsTo('App\User','user_id');
   	}

   	//Relation Many to One
   	////Many comments belong to a video
   	public function video(){
   		return $this->belongsTo('App\Video','video_id');
   	}
}
