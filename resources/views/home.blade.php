@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <!--
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
            </div>
        </div> 
        --->

        <div class="container">
            @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
            @endif

            <div id="videos-list">

                @foreach($videos as $video)
                <div class="video-item col-md-10 pull-left panel panel-default">
                    <div class="panel-body">
                        <!-- list images -->
                        @if(Storage::disk('images')->has($video->imagen))
                            <div class="video-imge-thumb col-md-3 pull-left">
                                <div class="video-image-mask">
                                    <img src="{{ url('/mini/'.$video->imagen)}}" class="video-image">
                                </div>
                            </div>
                                
                        @endif
                    

                        <div class="data">
                            <h4 class="video-title"> <a href="{{ route('detailVideo', ['videoId' => $video->id]) }}">{{ $video->title }}</a> </h4>
                            <p>
                                {{$video->user->name . ' ' . $video->user->surname}}
                            </p>
                        </div>

                        <br>
                        <br>

                        <!-- Action buttons-->
                        <a href="{{ route('detailVideo', ['videoId' => $video->id]) }}" class="btn btn-success">See</a>
                        @if(Auth::check() && Auth::user()->id == $video->user->id)
                            <a href="{{ route('videoEdit', ['video_id' => $video->id]) }}" class="btn btn-warning">Edit</a>
                            
                            <!-- Botón en HTML (lanza el modal en Bootstrap) -->
                            <a href="#modal{{$video->id}}" role="button" class="btn btn-primary" data-toggle="modal">Delete</a>
                              
                            <!-- Modal / Ventana / Overlay en HTML -->
                            <div id="modal{{$video->id}}" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">¿Are you sure?</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>¿Are you sure you want to delete this video?</p>
                                            <p class="text-warning"><small>{{$video->title}}</small></p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <a href="{{url('/delete-video/'.$video->id)}}" type="button" class="btn btn-danger">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                @endforeach

            </div>
        </div>

        {{ $videos->links() }}
    </div>
</div>
@endsection
