@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<h2> Edit {{$video->title}} </h2>
			<hr>
			<form action="{{ route('updateVideo', ['video_id' => $video->id]) }}" method="POST" enctype="multipart/form-data" class="col-lg-7">
				 {!! csrf_field() !!} <!-- Helper to CRSF attacks-->

				<!-- <input type="hidden" name="remember_token" value="{{ csrf_token() }}"> -->
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								<li>
									{{ $error }}
								</li>
							@endforeach
						</ul>
					</div>
				@endif

				<div class="form-group">
				 	<label for="title">Title</label>
				 	<input type="text" class="form-control" id="title" name="title"  value="{{ $video->title }}" />
				</div>
				<div class="form-group">
				 	<label for="description">Description</label>
				 	<textarea class="form-control" id="description" name="description">{{ $video->description }}</textarea>
				</div>
				<div class="form-group">
				 	<label for="image">Mini</label>
				 		@if(Storage::disk('images')->has($video->imagen))
                            <div class="video-imge-thumb">
                                <div class="video-image-mask">
                                    <img src="{{ url('/mini/'.$video->imagen)}}" class="video-image">
                                </div>
                            </div>
                                
                        @endif
				 	<input type="file" class="form-control" id="image" name="image" />
				</div>
				<div class="form-group">
				 	<label for="video">Video file</label>
					 	<video id="video-player" controls style="width: 650px; height: 360px; left: 0px; top: 0px;">
							<source src="{{ route('fileVideo', ['filename' => $video->video_path] ) }}">
							<!--<source src="{{ url('/video-file/'.$video->video_path)}}" > -->
							Your browser does not support HTML5 video.
						</video>
				 	<input type="file" class="form-control" id="video" name="video" />
				</div>
				<button type="submit" class="btn btn-success">Edit Video</button>
			</form>
		</div>
	</div>
@endsection