@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<h2> Make a video </h2>
		<hr>
		<form action="{{ route('saveVideo') }}" method="POST" enctype="multipart/form-data" class="col-lg-7">
			 {!! csrf_field() !!} <!-- Helper to CRSF attacks-->

			<!-- <input type="hidden" name="remember_token" value="{{ csrf_token() }}"> -->
			@if($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach($errors->all() as $error)
							<li>
								{{ $error }}
							</li>
						@endforeach
					</ul>
				</div>
			@endif

			<div class="form-group">
			 	<label for="title">Title</label>
			 	<input type="text" class="form-control" id="title" name="title"  value="{{ old('title') }}" />
			</div>
			<div class="form-group">
			 	<label for="description">Description</label>
			 	<textarea class="form-control" id="description" name="description">{{ old('description') }}</textarea>
			</div>
			<div class="form-group">
			 	<label for="image">Mini</label>
			 	<input type="file" class="form-control" id="image" name="image" />
			</div>
			<div class="form-group">
			 	<label for="video">Video file</label>
			 	<input type="file" class="form-control" id="video" name="video" />
			</div>
			<button type="submit" class="btn btn-success">Create Video</button>
		</form>
	</div>
	
</div>

@endsection