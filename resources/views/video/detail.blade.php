@extends('layouts.app')

@section('content')

	<div class="col-md-11 col-md-offset-1">
		<h2> {{ $video->title}}	</h2>
		<hr>
		<div class="col-md-8">
			<!-- Video -->

			<video id="video-player" controls style="width: 775px; height: 490px; left: 0px; top: 0px;">
				<source src="{{ route('fileVideo', ['filename' => $video->video_path] ) }}">
			<!--	<source src="{{ url('/video-file/'.$video->video_path)}}" > -->
				
				Your browser does not support HTML5 video.
			</video>
			<!--

			<video tabindex="-1" class="video-stream html5-main-video" controlslist="nodownload" style="width: 640px; height: 360px; left: 0px; top: 0px;" src="blob:https://www.youtube.com/205299ba-695b-4bf2-9017-a5f50e93f984">
				
			</video>
		-->
			<!-- Description -->
			<div class="panel panel-default video-data">
				<div class="panel-heading">
					<div class="panel-title">
						Uploaded by <strong>{{$video->user->name . ' ' . $video->user->surname}}</strong> at {{ \FormatTime::LongTimeFilter($video->created_at) }}
					</div>
				</div>
				<div class="panel-body">
					{{ $video->description}}
				</div>
			</div>
			<!-- Comments -->
			@if(Auth::check())
				@include('video.comments')
			@endif
		</div>

	</div>

@endsection